x56steam
========
idea
----

yoke --> driveByWire --> Autopilot --> driveByWire --> engineServo


Controls
--------

### drive by wire
**/controls/drive-by-wire/mode**	{0:off,1:autopilot,2:absolute,3:relative}

* 0: off : yoke input goes direct to the rockets
* 1: autopilot : controlled by numeric input
* 2: absolute : yoke input controls absolute target values for autopilot
* 3: relative : yoke input controls relative target values for autopilot

**/controls/engines/oms-enabled**
enables/disables oms engine servos

**/controls/engines/vel-enabled**
enables/disables velocity engine servos

**/controls/engines/velocity-pitch-target-deg**
target pitch in deg of the velocity engine

**/controls/engines/velocity-pitch-active**
enables/disables velocity engine pitch

**/controls/engines/velocity-yaw-target-deg**
target pitch in deg of the velocity engine

**/controls/engines/velocity-yaw-active**
enables/disables velocity engine yaw


**Throttle** : direct throttle of the velocity engine

**mixture** : pitch of the velocity engine

**Elevator** : drive by wire throttle of the pitch oms engine 

**Aileron** : drive by wire throttle of the roll oms engine

**Rudder** : direct throttle of the yaw oms engine

**propeller** : direct throttle of the oms engine 


Take off
--------

Move Throttle gentle and x56steam will take of vertical.






