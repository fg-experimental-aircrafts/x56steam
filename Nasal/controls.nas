var MyAxisHandler = func(prop){
	func(invert = 0) {
		var val = cmdarg().getNode("setting").getValue();
		setprop(prop,(1 - val) / 2);
	}
};
var MyEnginePitchHandler = func(prop){
	func(invert = 0) {
		var val = cmdarg().getNode("setting").getValue();
		val *= 180;
		val += 90;
		setprop(prop,val);
	}
};

#throttleAxis = MyAxisHandler("/controls/engines/throttle-target");
throttleAxis = MyAxisHandler("/controls/engines/vel-throttle-target");
mixtureAxis = MyEnginePitchHandler("/controls/engines/velocity-pitch-target-deg");
#propellerAxis = MyAxisHandler("/controls/engines/propeller-target");
propellerAxis = MyAxisHandler("/controls/engines/oms-throttle-target");


